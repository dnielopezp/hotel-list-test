/* eslint-disable */

import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import Hotels from '../components/Hoteles';

const Root = () => {
  return (
    <Router>
      <div>
        <div className="tabs">
          <div className="logo">Hotels Test</div>
          <div><Link href="/" to="/" >CRUD Hotels</Link></div>
        </div>
        <Switch>
          <Route exact path="/" component={Hotels} />
        </Switch>
      </div>

    </Router>
  );
};

export default Root;

