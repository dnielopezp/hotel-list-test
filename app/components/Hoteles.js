/* eslint-disable */

import React from 'react';

// Render function for item list
/**
 *
 * @param {*} propss
 */
const ItemList = (propss) => {
  const { name, cityVal, stars, priceV, images, _id } = propss.hotel;
  const starshotel = stars;
  // List images
  const listImages = images.map((image, key) => {
    const index = key + 1;
    return (<img key={index} src={image.uri} />);
  });
  return (
    <div className="item-list">
      <p>
        Hotel: {name} <br />
        City: {cityVal} <br />
        Stars: {starshotel} <br />
        Price from: {priceV} <br />
      </p>
      <div className="images">
       {listImages}
      </div>
      <button className="btUpdate" onClick={() => {propss.func(propss.hotel)}}>
        Update
      </button>
      <button className="btDelete" onClick={() => {propss.funcD(_id)}}>
        Delete
      </button>
    </div>
  );
};

class Hotels extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showPopUp: false,
      hotels: [],
      images: [],
      mImages: [],
      hotelUpdate: {}
    };
  }

  showPop = (hotel) => {
    document.getElementById('nameM').value = hotel.name;
    document.getElementById('cityM').value = hotel.cityVal;
    document.getElementById('starsM').value = hotel.stars;
    document.getElementById('priceM').value = hotel.priceV;
    let images = [];
    for(let aux in hotel.images) {
      images.push(hotel.images[aux]);
    }
    this.setState({
      showPopUp: true,
      hotelUpdate: hotel,
      mImages: images
    });
  };

  createNew(event) {
    let RC = this;
    event.preventDefault();
    fetch('http://localhost:3000/hotels', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        name: event.target.name.value,
        cityVal: event.target.cityVal.value,
        stars: event.target.stars.value,
        priceV: event.target.priceV.value,
        images: RC.state.images
      })
    }).then((res) => {
      // console.log(response);
      if (res.ok) {
        document.getElementById('createname').value = '';
        document.getElementById('city').value = '';
        document.getElementById('stars').value = '';
        document.getElementById('priceFrom').value = '';
        RC.setState({images: []})
        RC.getAll();
      }
    }).catch((response) => {
      // console.log(response);
    });
  };

  getAll () {
    let RC = this;
    fetch('http://localhost:3000/hotels', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      }
    }).then((response) => {
      return response.json();
    })
    .then((responseJson) => {
      RC.setState({hotels: responseJson.data});
    })
    .catch((response) => {
      // console.log(response);
    });
  };

  deleteItem(id) {
    let RC = this;
    fetch('http://localhost:3000/hotels', {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        'id': id
      })
    })
    .then((res) => {
      if (res.ok) {
        RC.getAll();
      }
    }).catch((response) => {
      // console.log(response);
    });
  };

  updateElement(event) {
    let RC = this;
    let nameV = document.getElementById('nameM').value;
    let cityV = document.getElementById('cityM').value;
    let starsV = document.getElementById('starsM').value;
    let priceV = document.getElementById('priceM').value;
    event.preventDefault();
    fetch('http://localhost:3000/hotels', {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        'id': RC.state.hotelUpdate._id,
        'name': nameV,
        'cityVal': cityV,
        'stars': starsV,
        'priceV': priceV,
        'images': RC.state.mImages
      })
    })
    .then((res) => {
      if (res.ok) {
        RC.getAll();
        RC.setState({showPopUp: false});
      }
    }).catch((response) => {
      // console.log(response);
    });
  };

  changeImg() {
    var RC = this;
    var images = [];
    images = RC.state.images;
    var file = document.getElementById('imageN').files[0];
    var reader  = new FileReader();

    if (file.size > 1000000) {
      alert('The maximum size of the file is until 1 Mb.');
      document.getElementById('imageN').value = null;
      return;
    }
    reader.onloadend = function () {
      images.push({uri: reader.result});
      RC.setState({images: images});
      document.getElementById('imageN').value = null;
    };

    if (file) {
      reader.readAsDataURL(file);
    }
  };

  changeImgM() {
    var RC = this;
    var images = [];
    images = RC.state.mImages;
    var file = document.getElementById('imageM').files[0];
    var reader  = new FileReader();

    if (file.size > 1000000) {
      alert('The maximum size of the file is until 1 Mb.');
      document.getElementById('imageM').value = null;
      return;
    }
    reader.onloadend = function () {
      images.push({uri: reader.result});
      RC.setState({mImages: images});
      document.getElementById('imageM').value = null;
    };

    if (file) {
      reader.readAsDataURL(file);
    }
  };

  deleteimgPr(index) {
    var images = this.state.images;
    images.splice((index - 1), 1);
    this.setState({images: images});
  };

  deleteimgPrM(index) {
    var images = this.state.mImages;
    images.splice((index - 1), 1);
    this.setState({mImages: images});
  };

  componentDidMount () {
    this.getAll();
  };

  render () {

    // List of hotels
    const listHotels = this.state.hotels.map((hotel, key) => {
      const index = key + 1;
      return (<ItemList hotel={hotel} key={index} func={this.showPop} funcD={this.deleteItem.bind(this)} />);
    });
    // List images
    const listImagesPreview = this.state.images.map((image, key) => {
      const index = key + 1;
      return (<img key={index} src={image.uri} onClick={() => {this.deleteimgPr(index)}} />);
    });
    const listImagesModify = this.state.mImages.map((image, key) => {
      const index = key + 1;
      return (<img key={index} src={image.uri} onClick={() => {this.deleteimgPrM(index)}} />);
    });

    return (
      <div className="content-page">
        <h2 id="heading">CRUD Hotels</h2>
        <h4>Create New</h4>
        <form className="formNewhotel" onSubmit={this.createNew.bind(this)}>
          <label>Fill with the information of the new Hotel</label>
          <br /><br />
          <input id="createname" type="text" name="name" placeholder="Hotel Name"  required />
          <input id="city" type="text" name="cityVal" placeholder="City" required />
          <input style={{width: '50px'}} id="stars" type="number" name="stars" placeholder="Stars" min="1" max="5" required />
          <input id="priceFrom" type="number" name="priceV" placeholder="Price from (USD)" min="0" required />
          <input type="file" id="imageN" name="imageN" accept=".png, .jpg, .jpeg" onChange={this.changeImg.bind(this)}></input>
          <input className="btCreate" type="submit" value="Create" />
        </form>
        <p>Images List: (To delete an image, click to it.)</p>
        <div className="previewImages">
          {listImagesPreview}
        </div>

        <hr />

        <h4>Hotels List</h4>
        <div>
          {listHotels}
        </div>

        <div className={(this.state.showPopUp ? 'popUp show' : 'popUp')}>
          <form className="formUpdatehotel" onSubmit={this.updateElement.bind(this)}>
            <h4>Update hotel</h4>
            <br />
            <label>Fill with the information of the Hotel</label>
            <br /><br />
            <input id="nameM" type="text" name="name" placeholder="Hotel Name"  required />
            <input id="cityM" type="text" name="cityVal" placeholder="City" required />
            <input style={{width: '50px'}} id="starsM" type="number" name="stars" placeholder="Stars" min="1" max="5" required />
            <input id="priceM" type="number" name="priceV" placeholder="Price from (USD)" min="0" required />
            <input type="file" id="imageM" name="imageM" accept=".png, .jpg, .jpeg" onChange={this.changeImgM.bind(this)}></input>
            <input className="btCreate" type="submit" value="Update" />

            <p>Images List: (To delete an image, click to it.)</p>
            <div className="previewImages">
              {listImagesModify}
            </div>
          </form>
          <button className="btClose" onClick={() => {this.setState({showPopUp: false})}}>(x) Close</button>
        </div>
      </div>
    );
  }
}

export default Hotels;
