const express = require('express');
const app = express();
const bodyParser= require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;
const path = require('path');
const fs = require('fs');

//Database mongodb
var db;

MongoClient.connect('mongodb://danieltest:passw0rd!@ds257627.mlab.com:57627/hotels-list', (err, database) => {
  if (err) return console.log(err);
  db = database.db('hotels-list');

  app.use(bodyParser.json({limit: '5mb'}));

  //Listen method where browsers can connect with us
  app.listen(3000, () => {
    console.log('listening on 3000');
  });

  /**
   * HOTELS METHODS-----------------------------------
   */

  // READ Get all hotels
  app.get('/hotels', (req, res) => {
    db.collection('hotels').find().toArray(function(err, results) {
      if (err) return res.send(false);
      res.json({data: results});
    });
  });

  // READ Get an information of specific hotel
  app.post('/hotel', (req, res) => {
    db.collection('hotels').findOne({_id: ObjectId(req.body.id)},
    (err, result) => {
      if (err) return res.send(500, err);
      res.json({data: result});
    });
  });

  // POST Create hotels
  app.post('/hotels', (req, res) => {
    db.collection('hotels').save(req.body, (err, result) => {
      if (err) return res.send(false);
      res.send(true);
    })
  });

  // DELETE hotel
  app.delete('/hotels', (req, res) => {
    db.collection('hotels').findOneAndDelete({_id: ObjectId(req.body.id)},
    (err, result) => {
      if (err) return res.send(500, err);
      res.send(true);
    });
  });

  // PUT Update hotel
  app.put('/hotels', (req, res) => {
    db.collection('hotels')
    .findOneAndUpdate({_id: ObjectId(req.body.id)}, {
      $set: {
        name: req.body.name,
        cityVal: req.body.cityVal,
        stars: req.body.stars,
        priceV: req.body.priceV,
        images: req.body.images
      }
    }, {}, (err, result) => {
      if (err) return res.send(err)
      res.send(result)
    })
  });

});
